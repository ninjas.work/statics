# About

Ninjas work is a platform where you get your web project done by professionals aka ninjas. Our ninjas are based on four ranks [Kage](#kage), [Jōnin](#jonin), [Chūnin](#chunin), [Genin](#genin) that gets your work done no matter what the development phase is or what you want to build online.


## Ranks

### Kage<a name="kage"></a>

Has to know a specific language and little bit of standard library for a hello world kind of application. He knows networking and internet protocol very well to build through learning a microservice. He facilitates DevOps, and curious about it. A Kage is aware of what high traffic does for a website and also curious of CyberSecurity. He studied of DDoS attacks and can provide documentation of a running website. Estimation is done by a Kage because he explored many projects and went deep also to bring outstanding insights. He is part of a team and always able to build confidence within the team by being aware of itself. He know to communicate with clients and has intended to be like an entrepreneur. He was a Jōnin and chosed Kage by a Kage socially on ninjas.work, he finished a production MVP for a client on ninjas.work


### Jōnin<a name="jonin"></a>

Knows the language and can build an algorithm in it. He knows to build a website with database known and others through learning. He knows the join, and knows how to use it by reading documentation. He can build a team of 3 and handle workloads of features for a web application. Indeed he could know also SEO, Rest, or cURL. He is comfortable in unix environments and loves Makefile. A Jonin is up to date with latest front-end frameworks and libraries like Angular, React, Vue and can easily understand DOM. He is aware of design patterns and knows to build a button class reflecting S.O.L.I.D principles.


### Chūnin<a name="chunin"></a>

Is a little bit focused on a language as a continuation and getting done in time. For Kage this is an ability to focus and adapt to routine.  A Chūnin can build a task from end to end but if they choose they can be lead a team of Genin.


### Genin<a name="genin"></a>

Is one who knows a project so he can build a feature in that language.
